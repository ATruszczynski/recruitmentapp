﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseInterface
{
    /// <summary>
    /// This class represents a single contact information of a given client. 
    /// Information held by this class is a single string, that is interpreted as either phone number or e-mail address, depending on the value of "ContactInfoType" property.
    /// </summary>
    [Table("ContactInfo")]
    public class ContactInfo
    {
        [Required, Key, Column("ContactInfoId")]
        public int ContactInfoId { get; set; }

        [Required, Column("ContactString")]
        public String ContactString { get; set; }

        [Required, Column("ContactInfoType")]
        public ContactInfoType ContactInfoType { get; set; }

        //defining foreign key in one-to-many relationship
        [Required, ForeignKey("Client"), Column("ClientId")]
        public int ClientId { get; set; }
        public Client Client { get; set; }

        public ContactInfo()
        {

        }
        public ContactInfo(int clientId, ContactInfoType cit)
        {
            ClientId = clientId;
            ContactInfoType = cit;
        }

        public override string ToString()
        {
            return ContactString;
        }
    }
}
