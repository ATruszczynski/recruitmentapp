﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.Entity.Infrastructure;

namespace DatabaseInterface
{
    /// <summary>
    /// Interface between business logic and underlying database. Realises Singleton design pattern.
    /// </summary>
    public class ClientDbInterface
    {
        #region Properties
        /// <summary>
        /// Client databse context.
        /// </summary>
        private ClientDbContext DbContext { get; set; }

        /// <summary>
        /// Instance of ClientDbInterface, that is exposed outside the class.
        /// </summary>
        private static ClientDbInterface Instance { get; set; } = null;

        /// <summary>
        /// Event that informs subscribes about changes in database.
        /// </summary>
        public event EventHandler DataBaseModified;

        #endregion



        private ClientDbInterface(string connectionString)
        {
            DbContext = new ClientDbContext(connectionString);
        }

        /// <summary>
        /// Function exposing instance of ClientDbInterface. Called first time, it requires a connection string to be provided.
        /// </summary>
        /// <param name="connectionString"> Connection string of database. </param>
        /// <returns></returns>
        public static ClientDbInterface GetInstance(String connectionString = null)
        {
            if (Instance == null && connectionString == null)
            {
                throw new ArgumentException("Initialisation requires connection string");
            }
            else if (Instance == null)
            {
                Instance = new ClientDbInterface(connectionString);
            }
            return Instance;
        }

        /// <summary>
        /// Sets instansce of singleton to null.
        /// </summary>
        public static void ClearContext()
        {
            Instance = null;
        }

        public void Touch()
        {
            DbContext.Clients.FirstOrDefault();
        }

        #region Addition
        /// <summary>
        /// Adds new client to database. Asynchronous.
        /// </summary>
        /// <param name="newClient"> Client to be added. </param>
        /// <remarks> To function properly, this function also adds new contact information associated with the new client. </remarks>
        public void AddClient(Client newClient)
        {
            new Task(() =>
            {
                try
                {
                    //retrive list of contact informations associated with new client (only one client can be added at a time, so all Entries with State == Added, are associated with new client)
                    Func<DbEntityEntry, bool> predicate = (entry) =>
                    {
                        return entry.Entity is ContactInfo && entry.State == EntityState.Added;
                    };

                    var listOfContactEntries = GetEntries(predicate).ToList();

                    List<ContactInfo> newContactInfos = new List<ContactInfo>();

                    foreach (var entery in listOfContactEntries)
                    {
                        newContactInfos.Add(entery.Entity as ContactInfo);
                    }

                    //Unintuitively, changes to the local version of database must be reverted, before proceeding. At this point, ContactInfo objects in newContactInfos hold nonsensical ClientId inside.
                    //The goal is to retrive proper ClientId from database, but for that to happen, SaveChanges() must first be called. But this would also save nonsenical ClientInfos (which throws expecption anyway)
                    //Therefore, changes to ContactInfos in local database are erased before saving changes, while preserving the newly created items in a temporary collection newContactInfos.
                    RollBackChanges();

                    DbContext.Clients.Add(newClient);
                    DbContext.SaveChanges();

                    int id = newClient.ClientId;

                    //give ClientInfos proper ClientId
                    foreach (var c in newContactInfos)
                    {
                        c.ClientId = id;
                        DbContext.ContactInfos.Add(c);
                    }

                    SaveChangesAndNotify();
                }
                catch (Exception e) //log exceptions if thrown
                {
                    LogError("Adding Client", e);
                }
            }).Start();

        }

        /// <summary>
        /// Marks contact info as added. Next call to SaveChanges() will add them to database.
        /// </summary>
        /// <param name="toAdd"></param>
        public void MarkContactInfoAsAdded(ContactInfo toAdd)
        {
            DbContext.ContactInfos.Add(toAdd);
        }

        #endregion



        #region Retrieval
        /// <summary>
        /// Retrives client based on their ClientId.
        /// </summary>
        /// <param name="searchedClientId"> Id of requested client. </param>
        /// <returns></returns>
        public Client GetClient(int searchedClientId)
        {
            return DbContext.Clients.Where(client => client.ClientId == searchedClientId).FirstOrDefault();
        }

        /// <summary>
        /// Retrieves all clients.
        /// </summary>
        /// <returns></returns>
        public List<Client> GetAllClients()
        {
            return DbContext.Clients.ToList();
        }

        /// <summary>
        /// Retrives all contact information associated with given client.
        /// </summary>
        /// <param name="clientId"> Id of client, whose information are being retrieved. </param>
        /// <returns></returns>
        public List<ContactInfo> GetClientContactInfos(int clientId)
        {
            return DbContext.ContactInfos.Where(ci => ci.ClientId == clientId).ToList();
        }

        /// <summary>
        /// Retrives all contact information.
        /// </summary>
        /// <returns></returns>
        public List<ContactInfo> GetAllClientContactInfos()
        {
            return DbContext.ContactInfos.ToList();
        }

        /// <summary>
        /// Retrieves all clients with theiir lists of contact information completed.
        /// </summary>
        /// <returns></returns>
        public List<Client> GetAllCompleteClients()
        {
            List<Client> clients = GetAllClients();
            List<ContactInfo> contactInformation = GetAllClientContactInfos();

            foreach(Client client in clients)
            {
                client.ContactInfos = contactInformation.Where(contactInfo => contactInfo.ClientId == client.ClientId).ToList();
            }
            return clients;
        }

        /// <summary>
        /// Converts a list of clients into a DataTable holding information about them.
        /// </summary>
        /// <param name="clients"> List of clients to convert. </param>
        /// <returns></returns>
        public static DataTable ClientListToDataTable(List<Client> clients)
        {
            DataTable result = new DataTable();

            //declare columns of DataTable
            result.Columns.Add(new DataColumn(ConstantsCollection.ClientIdColumnId, typeof(int)));
            result.Columns.Add(new DataColumn(ConstantsCollection.NameId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.SurnameId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.AddressId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.StatusId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.EmailId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.PhoneNumberId, typeof(string)));
            result.Columns.Add(new DataColumn(ConstantsCollection.EmailCount, typeof(int)));
            result.Columns.Add(new DataColumn(ConstantsCollection.PhoneNumberCount, typeof(int)));

            for (int i = 0; i < clients.Count; i++)
            {
                //create row for each client
                DataRow clientRow = result.NewRow();
                clientRow[ConstantsCollection.ClientIdColumnId] = clients[i].ClientId;
                clientRow[ConstantsCollection.NameId] = clients[i].Name;
                clientRow[ConstantsCollection.SurnameId] = clients[i].Surname;
                clientRow[ConstantsCollection.AddressId] = clients[i].Address;
                clientRow[ConstantsCollection.StatusId] = clients[i].Status;

                //find first email and phone number and put them into DataTable
                bool emailFound = false, phoneNumberFound = false;
                foreach (ContactInfo ci in clients[i].ContactInfos)
                {
                    if (ci.ContactInfoType == ContactInfoType.Email && !emailFound)
                    {
                        clientRow[ConstantsCollection.EmailId] = ci.ContactString;
                        emailFound = true;
                    }
                    if (ci.ContactInfoType == ContactInfoType.PhoneNumber && !phoneNumberFound)
                    {
                        clientRow[ConstantsCollection.PhoneNumberId] = ci.ContactString;
                        phoneNumberFound = true;
                    }
                    if (phoneNumberFound && emailFound)
                        break;
                }

                //if a type of contact information was not found, put a '-' in the DataTable cell
                if (!phoneNumberFound)
                {
                    clientRow[ConstantsCollection.PhoneNumberId] = ConstantsCollection.NoContactData;
                }
                if (!emailFound)
                {
                    clientRow[ConstantsCollection.EmailId] = ConstantsCollection.NoContactData;
                }

                //put total number of emails and phone numbers into the DataTable
                clientRow[ConstantsCollection.EmailCount] = clients[i].ContactInfos.ToList().FindAll(ci => ci.ContactInfoType == ContactInfoType.Email).Count;
                clientRow[ConstantsCollection.PhoneNumberCount] = clients[i].ContactInfos.ToList().FindAll(ci => ci.ContactInfoType == ContactInfoType.PhoneNumber).Count;
                
                result.Rows.Add(clientRow);
            }

            return result;
        }

        /// <summary>
        /// Retrives list of all clients and converts it into DataTable
        /// </summary>
        /// <returns></returns>
        public DataTable GetClientsFormatted()
        {
            return ClientListToDataTable(GetAllCompleteClients());
        }
        
        /// <summary>
        /// Gets database entries.
        /// </summary>
        /// <param name="predicate"> Predicate to filter the entries with.</param>
        /// <returns></returns>
        public IEnumerable<DbEntityEntry> GetEntries(Func<DbEntityEntry, bool> predicate = null)
        {
            IEnumerable<DbEntityEntry> result = DbContext.ChangeTracker.Entries();
            if(predicate != null)
            {
                result = result.Where(predicate);
            }
            return result;
        }
        
        #endregion



        #region Editing
        /// <summary>
        /// Edits existing client. Asynchronous.
        /// </summary>
        /// <param name="toEdit"></param>
        /// <remarks> This also saves newly added contact information. </remarks>
        public void EditClient(Client toEdit)
        {
            new Task(() =>
            {
                try
                {
                    //get list of database entires associated with added contact information
                    var changedEntries = GetEntries().Where(x => x.State == EntityState.Added).ToList();

                    //set newly created ContactInfo objects a proper ClientId
                    foreach (var entry in changedEntries)
                    {
                        if (entry.Entity is ContactInfo)
                        {
                            (entry.Entity as ContactInfo).ClientId = toEdit.ClientId;
                        }
                    }

                    SaveChangesAndNotify();
                }
                catch (Exception e) //log exception
                {
                    LogError("Editing", e);
                }
            }).Start();
        }

        #endregion



        #region Deletion

        /// <summary>
        /// Deletes client and all associated contact infos.
        /// </summary>
        /// <param name="toDelete">  Client object to be deleted. </param>
        public void DeleteClient(Client toDelete)
        {
            new Task(() =>
            {
                try
                {
                    //clear changes to be saved before proceeding (so that ClientInfo object added before saving changes, but during editing/deleting client, are not saved)
                    RollBackChanges();

                    int id = toDelete.ClientId;
                    DbContext.Clients.Remove(toDelete);

                    DbContext.ContactInfos.RemoveRange(DbContext.ContactInfos.ToList().FindAll(p => p.ClientId == id));

                    SaveChangesAndNotify();
                }
                catch (Exception e) //log exceptions
                {
                    LogError("Deleting", e);
                }
            }).Start();
        }

        /// <summary>
        /// Marks contact information for deleting. It will be deleted upon next SaveChanges() call.
        /// </summary>
        /// <param name="toDelete"> ContactInfo to be deleted. </param>
        public void MarkContactInfoAsDeleted(ContactInfo toDelete)
        {
            DbContext.ContactInfos.Remove(toDelete);
        }

        #endregion



        #region Other
        /// <summary>
        /// Marks all objects in database as unchanged. Next call to SaveChanges() will ignore added/modified/deleted items, effectivly dropping any changes made locally. Synchrnous.
        /// </summary>
        public void RollBackChanges()
        {
            //get list of all changed entries
            var changedEntries = GetEntries().Where(x => x.State != EntityState.Unchanged).ToList();

            //mark all entries in changedEntries as unchanged.
            foreach (var entry in changedEntries)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        {
                            entry.State = EntityState.Detached;
                            break;
                        }
                    default: //deleted, modified, or both
                        {
                            //"trick" database into thinking deleted objects were modified (as otherwise restoring original values doesn't work)
                            entry.State = EntityState.Modified;
                            //restore values in case object was modified
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        }
                }
            }
            DbContext.SaveChanges();
        }

        /// <summary>
        /// Logs error message connected with exception.
        /// </summary>
        /// <param name="prefix"> Prefix that will appear on the beginning of line in log file. </param>
        /// <param name="e"> Exception to be logged. </param>
        public void LogError(string prefix, Exception e)
        {
            StreamWriter sw = new StreamWriter("error.txt", true, Encoding.UTF8);
            sw.WriteLine("[" + prefix + " - " + DateTime.Now + "]: " + e.Message);
            sw.Flush();
        }

        /// <summary>
        /// Saves changes to database and notify subscribers of DataBaseModified event.
        /// </summary>
        private void SaveChangesAndNotify()
        {
            DbContext.SaveChanges();
            DataBaseModified?.Invoke(null, null);
        }

        #endregion

    }
}
