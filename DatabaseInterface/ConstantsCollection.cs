﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseInterface
{
    /// <summary>
    /// Static class storing some of the constants used in application.
    /// </summary>
    public static class ConstantsCollection
    {
        //strings used as keys in dictionary-like structures
        public static readonly String ClientIdColumnId = "ClientId";
        public static readonly String NameId = "Name";
        public static readonly String SurnameId = "Surname";
        public static readonly String AddressId = "Address";
        public static readonly String StatusId = "Status";
        public static readonly String EmailId = "Email";
        public static readonly String PhoneNumberId = "Phone Number";
        public static readonly String EmailCount = "EmailCount";
        public static readonly String PhoneNumberCount = "Phone number count";

        /// <summary>
        /// Path to file where database errors are to be logged.
        /// </summary>
        public static readonly String ErrorPath = "error.txt";
        /// <summary>
        /// Temporary, improper id used to initilaise objects, that may not be saved to database.
        /// </summary>
        public static readonly int mockId = -1;
        public static readonly String NoContactData = "-";


    }
}
