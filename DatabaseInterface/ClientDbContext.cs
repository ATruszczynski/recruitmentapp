﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseInterface
{
    /// <summary>
    /// This class provides database context for the client database.
    /// </summary>
    /// <remarks> Inherits after the DbContext class. </remarks>
    public class ClientDbContext: DbContext
    {
        /// <summary>
        /// List of clients in database.
        /// </summary>
        public DbSet<Client> Clients { get; set; }

        /// <summary>
        /// List of client contact information in database.
        /// </summary>
        public DbSet<ContactInfo> ContactInfos { get; set; }

        public ClientDbContext(String connectionString): base(connectionString)
        {

        }
    }
}
