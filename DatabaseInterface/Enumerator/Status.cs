﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseInterface
{
    /// <summary>
    /// Enumerator describing client status.
    /// </summary>
    public enum Status
    {
        Current,
        Potential
    }
}
