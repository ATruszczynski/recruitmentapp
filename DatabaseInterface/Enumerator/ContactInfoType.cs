﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseInterface
{
    /// <summary>
    /// Enumerator describing type of contact information.
    /// </summary>
    public enum ContactInfoType
    {
        Email,
        PhoneNumber
    }

    /// <summary>
    /// Class extending enum ContactInfoType
    /// </summary>
    public static class ContactInfoTypeMethods
    {
        /// <summary>
        /// Alternative to ToString() for this enum.
        /// </summary>
        /// <param name="contactInfoType"></param>
        /// <returns></returns>
        public static String GetString(this ContactInfoType contactInfoType)
        {
            switch (contactInfoType)
            {
                case ContactInfoType.Email:
                    return ConstantsCollection.EmailId;
                case ContactInfoType.PhoneNumber:
                    return ConstantsCollection.PhoneNumberId;
                default:
                    return "Error";
            }
        }
    }
}
