﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseInterface
{
    /// <summary>
    /// Class storing the basic data about clients.
    /// </summary>
    /// <remarks> Defines table in database. </remarks>
    [Table("ClientInfo")]
    public class Client
    {
        [Required, Key, Column("ClientId"), DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientId { get; set; }

        [Required, Column("Name")]
        public String Name { get; set; }

        [Required, Column("Surname")]
        public String Surname { get; set; }

        [Required, Column("Address")]
        public String Address { get; set; }

        [Required, Column("Status")]
        public Status Status { get; set; }

        //defines one-to-many realtion with contact inforamtion (email and phone number)
        public ICollection<ContactInfo> ContactInfos { get; set; }

        public Client()
        {
            
        }
    }
}
