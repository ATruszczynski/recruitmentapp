﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentApp
{
    /// <summary>
    /// Enumerator describing state of MainForm filters
    /// </summary>
    enum FilterState
    {
        OK,
        Warning,
        Error
    }
}
