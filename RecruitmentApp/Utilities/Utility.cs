﻿using DatabaseInterface;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecruitmentApp
{
    /// <summary>
    /// Class containing utility functions, used in at least two different classes. Also contains various string used in application.
    /// </summary>
    public static class Utility
    {
        /// <summary>
        /// Function determinig whether the string is valid, analysing its length and/or whether its first letter is capitalised.
        /// </summary>
        /// <param name="stringName"> String describing edited string. Used for validation error prompts. </param>
        /// <param name="sender"> TextEdit the validated string is from.</param>
        /// <param name="e"> CancelEventArgs holding information on whether the validaiton succeeded. </param>
        /// <param name="validateLength"> If true, it is determined whether string has length larger than 0.</param>
        /// <param name="validateCapitalisation">If true, it is determined whether string starts with capital letter.</param>
        /// <returns> Returns wheather the validation succeeded. </returns>
        /// <remarks> Function's returning of validation result is a convinience for designer. Forms themself use results of validation stored in CancelEventArgs. </remarks>
        public static bool ValidateString(String stringName, object sender, CancelEventArgs e, bool validateLength = true, bool validateCapitalisation = true)
        {
            TextEdit sourceTextEdit = sender as TextEdit;
            string stringToValidate = sourceTextEdit.Text;

            //e.Cancel = true signalises failure of validation
            if (validateLength && stringToValidate.Length == 0)
            {
                e.Cancel = true;
                sourceTextEdit.ErrorText = stringName + MustContainAtLeastOneCharacter;
                return false;
            }
            if (validateCapitalisation && !char.IsUpper(stringToValidate[0]))
            {
                e.Cancel = true;
                sourceTextEdit.ErrorText = stringName + ShouldStartWithCapitalLetter;
                return false;
            }
            if(!char.IsLetterOrDigit(stringToValidate[0]))
            {
                e.Cancel = true;
                sourceTextEdit.ErrorText = stringName + DontStartWithWhiteSpace;
                return false;
            }
            return true;
        }


        //strings used by forms
        public static readonly String MustContainAtLeastOneCharacter = " must contain at least one character!";
        public static readonly String ShouldStartWithCapitalLetter = " should start with capital letter!";
        public static readonly String FilterError = "Error: At least one filter value is incorrect.";
        public static readonly String FilterWarning_Pre = "Warning: ";
        public static readonly String FilterWarning_Suf = " filter values don't " + Environment.NewLine + "make sense and they are ingored.";
        public static readonly String AddClientFormTitle = "Add client";
        public static readonly String EditClientFormTitle = "Edit client";
        public static readonly String DontStartWithWhiteSpace = " must start with letter or number.";
    }
}
