﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DatabaseInterface;

namespace RecruitmentApp
{
    /// <summary>
    /// Class defining the dialog used to modify the contact information.
    /// </summary>
    public partial class ChangeContactInfoForm : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Currently edited contact information
        /// </summary>
        public ContactInfo EditedContactInfo { get; set; }
        /// <summary>
        /// Varaible storing information wheater the form's text editor validation has succeeded.
        /// </summary>
        bool Valid { get; set; } = false;

        /// <summary>
        /// Constructor of ChangeContactInfoForm.
        /// </summary>
        /// <param name="toEdit"> Contact information to be edited. If toEdit is null, new contact information is created.</param>
        /// <param name="newContactInfoType"> If new ContactInfo is created, newContactInfoType defines which type will it be. </param>
        public ChangeContactInfoForm(ContactInfo toEdit = null, ContactInfoType newContactInfoType = ContactInfoType.Email)
        {
            InitializeComponent();

            

            if(toEdit == null)
            {
                //disable delete button during creation of new ContactInfo
                deleteButton.Visible = false;
                EditedContactInfo = new ContactInfo(ConstantsCollection.mockId, newContactInfoType);
            }
            else
            {
                EditedContactInfo = toEdit;
                contactInfoTextEdit.Text = EditedContactInfo.ContactString;
            }

            this.Text += EditedContactInfo.ContactInfoType.GetString();
            changeContactInfoLabel.Text += EditedContactInfo.ContactInfoType.GetString() + ":";
            //change string in window to reflect what type of contact information is being changed
            if (EditedContactInfo.ContactInfoType == ContactInfoType.PhoneNumber)
            {
                contactInfoTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
                contactInfoTextEdit.Properties.Mask.EditMask = "\\d+";
            }
        }

        /// <summary>
        /// Validating input in form's TextEdit. If validaiton succeeds, content of edited contact information is changed.
        /// </summary>
        /// <param name="sender"> TextEdit that is being validated. </param>
        /// <param name="e"> Paramter storing information about validation success </param>
        private void contactInfoTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = sender as TextEdit;
            if(Utility.ValidateString(EditedContactInfo.ContactInfoType.ToString(), sender, e, validateCapitalisation: false))
            {
                Valid = true;
                EditedContactInfo.ContactString = te.Text;
            }
        }

        /// <summary>
        /// Reaction to clicking "Save" button by user. Forces form's TextEdit to validate. If validation succeeds, closes the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            Valid = false;
            contactInfoTextEdit.IsModified = true;
            contactInfoTextEdit.DoValidate();

            if (Valid)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        /// <summary>
        /// Close form. Set DialogResult == Cancel to indicate, that changes to contact information are to be discarded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Close form. Set DialogResult == Abort to indicate, that changed contact information is to be deleted.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e)
        {
            //DialogResult.Abort is used as an information to parent form, that edited ContactInfo is to be deleted.
            DialogResult = DialogResult.Abort;
            Close();
        }
    }
}