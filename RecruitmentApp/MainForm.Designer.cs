﻿namespace RecruitmentApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addNewClientButton = new DevExpress.XtraEditors.SimpleButton();
            this.clientGrid = new DevExpress.XtraGrid.GridControl();
            this.clientGridList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.filtersGroupControl = new DevExpress.XtraEditors.GroupControl();
            this.filterResetButton = new DevExpress.XtraEditors.SimpleButton();
            this.filterErrorWarningLabel = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.minPhoneNumbersTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.maxEmailTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.maxPhoneNumbersTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.minEmailsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.clientListLabel = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.clientGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientGridList)).BeginInit();
            this.mainLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filtersGroupControl)).BeginInit();
            this.filtersGroupControl.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minPhoneNumbersTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxEmailTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPhoneNumbersTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minEmailsTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // addNewClientButton
            // 
            this.addNewClientButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addNewClientButton.Location = new System.Drawing.Point(443, 1);
            this.addNewClientButton.Name = "addNewClientButton";
            this.addNewClientButton.Size = new System.Drawing.Size(94, 29);
            this.addNewClientButton.TabIndex = 0;
            this.addNewClientButton.Text = "Add new client";
            this.addNewClientButton.Click += new System.EventHandler(this.addNewClientButton_Click);
            // 
            // clientGrid
            // 
            this.clientGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientGrid.Location = new System.Drawing.Point(3, 33);
            this.clientGrid.MainView = this.clientGridList;
            this.clientGrid.Name = "clientGrid";
            this.clientGrid.Size = new System.Drawing.Size(534, 424);
            this.clientGrid.TabIndex = 0;
            this.clientGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.clientGridList});
            // 
            // clientGridList
            // 
            this.clientGridList.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.clientGridList.GridControl = this.clientGrid;
            this.clientGridList.Name = "clientGridList";
            this.clientGridList.OptionsBehavior.Editable = false;
            this.clientGridList.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.clientGridList.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.clientGridList.OptionsView.ShowGroupPanel = false;
            this.clientGridList.DoubleClick += new System.EventHandler(this.clientGridList_DoubleClick);
            // 
            // mainLayoutPanel
            // 
            this.mainLayoutPanel.ColumnCount = 1;
            this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Controls.Add(this.clientGrid, 0, 1);
            this.mainLayoutPanel.Controls.Add(this.panelControl2, 0, 2);
            this.mainLayoutPanel.Controls.Add(this.clientListLabel, 0, 0);
            this.mainLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainLayoutPanel.Name = "mainLayoutPanel";
            this.mainLayoutPanel.RowCount = 3;
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 205F));
            this.mainLayoutPanel.Size = new System.Drawing.Size(540, 665);
            this.mainLayoutPanel.TabIndex = 0;
            // 
            // panelControl2
            // 
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.addNewClientButton);
            this.panelControl2.Controls.Add(this.filtersGroupControl);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 460);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(540, 205);
            this.panelControl2.TabIndex = 4;
            // 
            // filtersGroupControl
            // 
            this.filtersGroupControl.Controls.Add(this.filterResetButton);
            this.filtersGroupControl.Controls.Add(this.filterErrorWarningLabel);
            this.filtersGroupControl.Controls.Add(this.tableLayoutPanel1);
            this.filtersGroupControl.Location = new System.Drawing.Point(3, 1);
            this.filtersGroupControl.Name = "filtersGroupControl";
            this.filtersGroupControl.Size = new System.Drawing.Size(384, 200);
            this.filtersGroupControl.TabIndex = 0;
            this.filtersGroupControl.Text = "Filters:";
            // 
            // filterResetButton
            // 
            this.filterResetButton.Location = new System.Drawing.Point(272, 130);
            this.filterResetButton.Name = "filterResetButton";
            this.filterResetButton.Size = new System.Drawing.Size(94, 29);
            this.filterResetButton.TabIndex = 2;
            this.filterResetButton.Text = "Reset";
            this.filterResetButton.Click += new System.EventHandler(this.filterResetButton_Click);
            // 
            // filterErrorWarningLabel
            // 
            this.filterErrorWarningLabel.Appearance.ForeColor = System.Drawing.Color.Red;
            this.filterErrorWarningLabel.Appearance.Options.UseForeColor = true;
            this.filterErrorWarningLabel.Location = new System.Drawing.Point(5, 130);
            this.filterErrorWarningLabel.Name = "filterErrorWarningLabel";
            this.filterErrorWarningLabel.Size = new System.Drawing.Size(84, 16);
            this.filterErrorWarningLabel.TabIndex = 1;
            this.filterErrorWarningLabel.Text = "<placeholder>";
            this.filterErrorWarningLabel.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Controls.Add(this.minPhoneNumbersTextEdit, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.maxEmailTextEdit, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.maxPhoneNumbersTextEdit, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.minEmailsTextEdit, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(360, 95);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // minPhoneNumbersTextEdit
            // 
            this.minPhoneNumbersTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minPhoneNumbersTextEdit.Location = new System.Drawing.Point(123, 65);
            this.minPhoneNumbersTextEdit.Name = "minPhoneNumbersTextEdit";
            this.minPhoneNumbersTextEdit.Size = new System.Drawing.Size(110, 22);
            this.minPhoneNumbersTextEdit.TabIndex = 6;
            // 
            // maxEmailTextEdit
            // 
            this.maxEmailTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxEmailTextEdit.Location = new System.Drawing.Point(241, 35);
            this.maxEmailTextEdit.Name = "maxEmailTextEdit";
            this.maxEmailTextEdit.Size = new System.Drawing.Size(114, 22);
            this.maxEmailTextEdit.TabIndex = 5;
            // 
            // maxPhoneNumbersTextEdit
            // 
            this.maxPhoneNumbersTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxPhoneNumbersTextEdit.Location = new System.Drawing.Point(241, 65);
            this.maxPhoneNumbersTextEdit.Name = "maxPhoneNumbersTextEdit";
            this.maxPhoneNumbersTextEdit.Size = new System.Drawing.Size(114, 22);
            this.maxPhoneNumbersTextEdit.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(5, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "Phone numbers:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(241, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Max:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(5, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "Emails:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(123, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Min:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // minEmailsTextEdit
            // 
            this.minEmailsTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minEmailsTextEdit.Location = new System.Drawing.Point(123, 35);
            this.minEmailsTextEdit.Name = "minEmailsTextEdit";
            this.minEmailsTextEdit.Size = new System.Drawing.Size(110, 22);
            this.minEmailsTextEdit.TabIndex = 4;
            // 
            // clientListLabel
            // 
            this.clientListLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.clientListLabel.Location = new System.Drawing.Point(3, 7);
            this.clientListLabel.Name = "clientListLabel";
            this.clientListLabel.Size = new System.Drawing.Size(43, 16);
            this.clientListLabel.TabIndex = 5;
            this.clientListLabel.Text = "Clients:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 665);
            this.Controls.Add(this.mainLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(550, 700);
            this.Name = "MainForm";
            this.Text = "X Clients";
            ((System.ComponentModel.ISupportInitialize)(this.clientGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientGridList)).EndInit();
            this.mainLayoutPanel.ResumeLayout(false);
            this.mainLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.filtersGroupControl)).EndInit();
            this.filtersGroupControl.ResumeLayout(false);
            this.filtersGroupControl.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.minPhoneNumbersTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxEmailTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxPhoneNumbersTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minEmailsTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton addNewClientButton;
        private DevExpress.XtraGrid.GridControl clientGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView clientGridList;
        private System.Windows.Forms.TableLayoutPanel mainLayoutPanel;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.GroupControl filtersGroupControl;
        private DevExpress.XtraEditors.TextEdit minPhoneNumbersTextEdit;
        private DevExpress.XtraEditors.TextEdit maxEmailTextEdit;
        private DevExpress.XtraEditors.TextEdit maxPhoneNumbersTextEdit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit minEmailsTextEdit;
        private DevExpress.XtraEditors.SimpleButton filterResetButton;
        private DevExpress.XtraEditors.LabelControl filterErrorWarningLabel;
        private DevExpress.XtraEditors.LabelControl clientListLabel;
    }
}