﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DatabaseInterface;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace RecruitmentApp
{
    /// <summary>
    /// Class defining dialog used to modify information about client.
    /// </summary>
    public partial class ClientModificationForm : DevExpress.XtraEditors.XtraForm
    {
        #region Properties
        /// <summary>
        /// Client that is currently edited by dialog.
        /// </summary>
        public Client editedClient { get; set; }

        /// <summary>
        /// Varaible storing information wheater the form's text editors' validation has succeeded.
        /// </summary>
        bool valid { get; set; } = true;

        /// <summary>
        /// A handy list of all text editors in form.
        /// </summary>
        List<TextEdit> textEdits { get; set; }

        /// <summary>
        /// Variable used to circumvent problem of validation activating too early in form's life cycle.
        /// </summary>
        bool validaitonEnabled { get; set; } = false;

        /// <summary>
        /// Form's interface for interacting with database
        /// </summary>
        ClientDbInterface clientDbInterface { get; set; }

        #endregion



        /// <summary>
        /// Main contructor of ClientModificationForm.
        /// </summary>
        /// <param name="toEdit"> Client to be edited. If toEdit == null, new client is created. </param>
        public ClientModificationForm(Client toEdit = null)
        {
            InitializeComponent();

            clientDbInterface = ClientDbInterface.GetInstance();

            if (toEdit == null)
            {
                //disabling delete button during creation of new client
                this.Text = Utility.AddClientFormTitle;
                deleteButton.Visible = false;
                editedClient = new Client();
            }
            else
            {
                this.Text = Utility.EditClientFormTitle;
                editedClient = toEdit;
                //supply editedClient with list of all of its contact information
                editedClient.ContactInfos = clientDbInterface.GetClientContactInfos(editedClient.ClientId);
            }

            //initilise form's drop-down list items
            statusComboBoxEdit.Properties.Items.AddRange(new List<Status>() { Status.Current, Status.Potential });

            //set up form's TextEdits list
            textEdits = new List<TextEdit>() { nameTextEdit, surnameTextEdit, addressTextEdit};

            UpdateForm();

        }

        /// <summary>
        /// Sets focus on the "Save" button and activates validation of form's TextEdits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientModificationForm_Shown(object sender, EventArgs e)
        {
            saveButton.Focus();
            validaitonEnabled = true;
        }


        #region Utility functions
        /// <summary>
        /// Fills all editors in form with information about currently edited client.
        /// </summary>
        private void UpdateForm()
        {
            nameTextEdit.Text = editedClient.Name;
            surnameTextEdit.Text = editedClient.Surname;
            addressTextEdit.Text = editedClient.Address;
            statusComboBoxEdit.SelectedItem = editedClient.Status;

            //A predicate used to retrieve from database all contact information that are currently assosicated with edited client. It will not fetch items marked for deleting and it 
            //will fetch items associated with mockId (as all of them are created during editing of current client).
            Func<DbEntityEntry, bool> predicate = (entry) =>
            {
                if(entry.Entity is ContactInfo)
                {
                    ContactInfo current = entry.Entity as ContactInfo;
                    return entry.State != EntityState.Deleted &&
                           (current.ClientId == editedClient.ClientId ||
                            current.ClientId == ConstantsCollection.mockId);
                }
                return false;
            };
            var entriesToShow = clientDbInterface.GetEntries(predicate);

            //separate all fetched contact information into two lists - one of emails and one of phone numbers
            List<object> emails = new List<object>();
            List<object> phoneNumbers = new List<object>();

            foreach(var entry in entriesToShow)
            {
                ContactInfo ci = entry.Entity as ContactInfo;
                if(ci.ContactInfoType == ContactInfoType.Email)
                {
                    emails.Add(ci);
                }
                else
                {
                    phoneNumbers.Add(ci);
                }
            }

            emailListBoxControl.Items.Clear();
            phoneNumberListBoxControl.Items.Clear();

            if(emails.Count == 0) //set up an empty list in editor
            {
                emailListBoxControl.Items.Add("<no emails>");
                emailListBoxControl.Enabled = false;
            }
            else //add all elements to list in editor
            {
                emailListBoxControl.Items.AddRange(emails.ToArray());
                emailListBoxControl.Enabled = true;
            }
            if(phoneNumbers.Count == 0) //analogical
            {
                phoneNumberListBoxControl.Items.Add("<phone number>");
                phoneNumberListBoxControl.Enabled = false;
            }
            else
            {
                phoneNumberListBoxControl.Items.AddRange(phoneNumbers.ToArray());
                phoneNumberListBoxControl.Enabled = true;
            }
        }

        /// <summary>
        /// Shows dialog for adding new client contact information. 
        /// </summary>
        /// <param name="clientInfoType">  Type of contact information to be added. </param>
        /// <remarks> If dialog returns with DialogResult.OK, mark contact information as added. </remarks>
        private void AddContactInfo(ContactInfoType clientInfoType)
        {
            ChangeContactInfoForm addNewContactInfoForm = new ChangeContactInfoForm(null, clientInfoType);

            switch (addNewContactInfoForm.ShowDialog())
            {
                case DialogResult.OK:
                    {
                        clientDbInterface.MarkContactInfoAsAdded(addNewContactInfoForm.EditedContactInfo);
                        break;
                    }
            }
            UpdateForm();
        }

        /// <summary>
        /// Shows dialog for editing client contact information. 
        /// </summary>
        /// <param name="listBoxControl"> ListBox which element was clicked. </param>
        /// <param name="e"> Argument storing information used to retrieve clicked row. </param>
        /// <remarks> If dialog returns with DialogResult.Abort, mark contact information as deleted. </remarks>
        private void EditContactInfo(ListBoxControl listBoxControl, MouseEventArgs e)
        {
            ContactInfo chosenContactInfo;
            //retrive clicked list element
            try
            {
                chosenContactInfo = (ContactInfo)listBoxControl.Items[listBoxControl.IndexFromPoint(e.Location)];
            }
            catch(ArgumentOutOfRangeException ee) //user clicked on list where there is no item
            {
                return;
            }

            ChangeContactInfoForm editContactInfoDialog = new ChangeContactInfoForm(chosenContactInfo);

            switch (editContactInfoDialog.ShowDialog())
            {
                case DialogResult.Abort:
                    {
                        clientDbInterface.MarkContactInfoAsDeleted(chosenContactInfo);
                        break;
                    }
            }
            UpdateForm();
        }

        #endregion



        #region Reacting to editors' changes
        /// <summary>
        /// Validating input of client's name.
        /// </summary>
        /// <param name="sender"> TextEdit assosciated with client's name. </param>
        /// <param name="e"> Paramter storing information about validation success. </param>
        private void nameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            string text = (sender as TextEdit).Text;
            if (validaitonEnabled && Utility.ValidateString("Name", sender, e))
            {
                editedClient.Name = text;
                valid = true;
            }
        }
        /// <summary>
        /// Validating input of client's surname.
        /// </summary>
        /// <param name="sender"> TextEdit assosciated with client's surnname </param>
        /// <param name="e"> Paramter storing information about validation success. </param>
        private void surnameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            string text = (sender as TextEdit).Text;
            if (validaitonEnabled && Utility.ValidateString("Surname", sender, e))
            {
                editedClient.Surname = text;
                valid = true;
            }
        }

        /// <summary>
        /// Validating input of client's address.
        /// </summary>
        /// <param name="sender"> TextEdit assosciated with client's address </param>
        /// <param name="e"> Paramter storing information about validation success. </param>
        private void addressTextEdit_Validating(object sender, CancelEventArgs e)
        {
            string text = (sender as TextEdit).Text;
            if (validaitonEnabled && Utility.ValidateString("Address", sender, e, validateCapitalisation: false))
            {
                editedClient.Address = text;
                valid = true;
            }
        }

        /// <summary>
        /// Reaction to changes on the drop-down list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void statusComboBoxEdit_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit statusComboBox = sender as ComboBoxEdit;
            editedClient.Status = (Status)statusComboBox.SelectedItem;
        }

        /// <summary>
        /// Reaction to user wanting to add new email.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addEmailButton_Click(object sender, EventArgs e)
        {
            AddContactInfo(ContactInfoType.Email);
        }

        /// <summary>
        /// Reaction to user wanting to add new phone number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addPhoneNumberButton_Click(object sender, EventArgs e)
        {
            AddContactInfo(ContactInfoType.PhoneNumber);
        }

        /// <summary>
        /// Reaction to user wanting to edit e-mail.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void emailListBoxControl_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditContactInfo(sender as ListBoxControl, e);
        }

        /// <summary>
        /// Reaction to user wanting to edit phone number.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void phoneNumberListBoxControl_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            EditContactInfo(sender as ListBoxControl, e);
        }

        #endregion

        
        
        #region Closing form
        /// <summary>
        /// Validates all TextEdits. If they are validated successfully, closes the form with DialogResult == DialogResult.OK, which
        /// indicates to parent form, that all changes should be saved in database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_Click(object sender, EventArgs e)
        {
            foreach(var textEdit in textEdits)
            {
                valid = false;
                textEdit.IsModified = true;
                textEdit.DoValidate();
            }
            if (valid)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        /// <summary>
        /// Closes dialog. Sets DialogResult == Cancel to send parent form information that all changes to edited client are to be ignored.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        /// <summary>
        /// Closes dialog. Sets DialogResult == Abort to send parent form information, that edited client should be removed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
            Close();
        }

        #endregion


    }
}