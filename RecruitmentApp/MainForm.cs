﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DatabaseInterface;
using ColumnView = DevExpress.XtraGrid.Views.Base.ColumnView;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Data.Entity;

namespace RecruitmentApp
{
    /// <summary>
    /// Main form of the program.
    /// </summary>
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        #region Properties
        /// <summary>
        /// Form's interface for interacting with database.
        /// </summary>
        ClientDbInterface ClientDbInterface { get; set; }

        /// <summary>
        /// Handy list of TextEdits associated with list filters.
        /// </summary>
        List<TextEdit> FilterTextEditors { get; set; }

        /// <summary>
        /// Variable holding information whether there have been a filter error.
        /// </summary>
        FilterState FilterState { get; set; } = FilterState.OK;

        object bLock = null;
        bool initiated = false;

        #endregion



        public MainForm()
        {
            InitializeComponent();

            //attempt to open database connection. Log error if it fails
            try
            {
                ClientDbInterface = ClientDbInterface.GetInstance("name=ClientDbConnectionString");
                ClientDbInterface.Touch();
            }
            catch (Exception e)
            {
                ClientDbInterface.LogError("Init", e);
                Close();
            }

            //set up a list of TextEdits providing values for filters
            FilterTextEditors = new List<TextEdit>() { minEmailsTextEdit, maxEmailTextEdit, minPhoneNumbersTextEdit, maxPhoneNumbersTextEdit };

            //set up form's reaction to filter TextEdits changes
            foreach (var te in FilterTextEditors)
            {
                te.EditValueChanged += UptadeFilterString;
            }

            //set up form's reaction to database changes
            ClientDbInterface.DataBaseModified += UpdateGrid;

            UpdateGrid();
        }



        #region Reacting to user input
        /// <summary>
        /// Shows form for adding a new client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addNewClientButton_Click(object sender, EventArgs e)
        {
            ClientModificationForm addNewClientDialog = new ClientModificationForm();

            switch (addNewClientDialog.ShowDialog())
            {
                case DialogResult.OK:
                    {
                        ClientDbInterface.AddClient(addNewClientDialog.editedClient);
                        break;
                    }
                case DialogResult.Cancel:
                    {
                        break;
                    }
            }
        }

        /// <summary>
        /// Shows form for editing existing client.
        /// </summary>
        /// <param name="sender"> Grid holding list of clients. </param>
        /// <param name="e"></param>
        private void clientGridList_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs mouseEventArgs = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(mouseEventArgs.Location);

            if (info.InRow || info.InRowCell) //did user double-click in a grid row?
            {
                //get the row clicked
                DataRowView rowView = view.GetRow(info.RowHandle) as DataRowView;
                var row = rowView.Row;

                //as grid doesn't hold Client objects inside, retrive the Client whose ClientId was associated with clicked row
                Client clientToEdit = ClientDbInterface.GetClient((int)row[ConstantsCollection.ClientIdColumnId]);

                ClientModificationForm clientEditForm = new ClientModificationForm(clientToEdit);

                switch (clientEditForm.ShowDialog())
                {
                    case DialogResult.OK: //save changes
                        {
                            ClientDbInterface.EditClient(clientEditForm.editedClient);
                            break;
                        }
                    case DialogResult.Cancel: //discard
                        {
                            ClientDbInterface.RollBackChanges();
                            break;
                        }
                    case DialogResult.Abort: //delete the client
                        {
                            ClientDbInterface.DeleteClient(clientEditForm.editedClient);
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Updates filter on client list, when TextEdits associated with filters have their content changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UptadeFilterString(object sender, EventArgs e)
        {
            //nullable ints holding information about demanded filtering
            int? minEmail = InterpretTextEditContent(minEmailsTextEdit);
            int? maxEmail = InterpretTextEditContent(maxEmailTextEdit);
            int? minPhoneNum = InterpretTextEditContent(minPhoneNumbersTextEdit);
            int? maxPhoneNum = InterpretTextEditContent(maxPhoneNumbersTextEdit);
            
            string filterString = "";

            //string in form '[<Column Name>]' to use further 
            string emailBracket = "[" + ConstantsCollection.EmailCount + "]";
            string phoneBracket = "[" + ConstantsCollection.PhoneNumberCount + "]";

            //list of partial filter strings to merge into final one
            List<string> toMerge = new List<string>();

            //display warning if filter values don't make sense
            if (minEmail != null && maxEmail != null && minEmail.Value > maxEmail.Value)
            {
                ShowWarningLabel("emails'");
                minEmail = maxEmail = null;
            }
            if (minPhoneNum != null && maxPhoneNum != null && minPhoneNum.Value > maxPhoneNum.Value)
            {
                ShowWarningLabel("phone numbers'");
                minPhoneNum = maxPhoneNum = null;
            }

            //add partial filter strings to toMerge
            if (minEmail != null)
                toMerge.Add(emailBracket + " >= " + minEmail.Value);
            if (maxEmail != null)
                toMerge.Add(emailBracket + " <= " + maxEmail.Value);
            if (minPhoneNum != null)
                toMerge.Add(phoneBracket + " >= " + minPhoneNum.Value);
            if (maxPhoneNum != null)
                toMerge.Add(phoneBracket + " <= " + maxPhoneNum.Value);

            //concatenate partial strings
            if (toMerge.Count > 0)
            {
                filterString = toMerge[0];
                toMerge.RemoveAt(0);
                foreach (var te in toMerge)
                {
                    filterString += " And " + te; //taking intersection of sets defined by partial strings
                }
            }

            //set the filter string
            (clientGrid.Views[0] as GridView).ActiveFilterString = filterString;

            //reset filter errors
            FilterState = FilterState.OK;
        }

        /// <summary>
        /// Resets all TextEdits associated with filters and resets filter state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filterResetButton_Click(object sender, EventArgs e)
        {
            (clientGrid.Views[0] as GridView).ActiveFilterString = "";
            FilterState = FilterState.OK;
            foreach (TextEdit te in FilterTextEditors)
            {
                te.BackColor = Color.White;
                te.Text = "";
            }
        }

        #endregion



        #region Utility functions
        /// <summary>
        /// Provides client grid (list) with data.
        /// </summary>
        /// <param name="ignored1"></param>
        /// <param name="ignored2"></param>
        /// <remarks> Columns 0, 7 and 8 are utility columns and they are hidden in the displayed grid. </remarks>
        private void UpdateGrid(object ignored1 = null, EventArgs ignored2 = null)
        {
            DataTable clientDataTable = ClientDbInterface.GetClientsFormatted();
            clientGrid.DataSource = clientDataTable;

            //hide utility columns
            GridView gridView = clientGrid.Views[0] as GridView;
            gridView.Columns[0].Visible = false;
            gridView.Columns[7].Visible = false;
            gridView.Columns[8].Visible = false;

            clientGrid.RefreshDataSource();
        }

        /// <summary>
        /// Converts text from TextEdits associated with filters into values usable in UptadeFilterString()
        /// </summary>
        /// <param name="textEdit"></param>
        /// <returns></returns>
        private int? InterpretTextEditContent(TextEdit textEdit)
        {
            string toInterpret = textEdit.Text;
            int result = -1;

            //reset state of analysed TextEdit
            textEdit.BackColor = Color.White;
            ResetLabel();

            if (toInterpret == "" || toInterpret == "-") //values that mean no filtering
            {
                return null;
            }
            if(int.TryParse(toInterpret, out result)) //attempt to read integer
            {
                return result;
            }
            else //interpretation failed
            {
                //mark TextEdit as containing incorrerct value and display error message
                textEdit.BackColor = Color.Red;
                ShowErrorLabel();
                return null;
            }
        }

        /// <summary>
        /// Displays warning label.
        /// </summary>
        /// <param name="indentificator"> String identifing which filter has incorrect values. </param>
        /// <remarks> If error label is already displayed, warning label does not override it. </remarks>
        private void ShowWarningLabel(string indentificator)
        {
            if (FilterState != FilterState.Error)
            {
                filterErrorWarningLabel.Visible = true;
                filterErrorWarningLabel.Text = Utility.FilterWarning_Pre + indentificator + Utility.FilterWarning_Suf;
            }
        }

        /// <summary>
        /// Displays error label.
        /// </summary>
        private void ShowErrorLabel()
        {
            filterErrorWarningLabel.Visible = true;
            FilterState = FilterState.Error;
            filterErrorWarningLabel.Text = Utility.FilterError;
        }

        /// <summary>
        /// Resets warning/error label.
        /// </summary>
        private void ResetLabel()
        {
            if(FilterState == FilterState.OK)
                filterErrorWarningLabel.Visible = false;
        }

        #endregion
    }
}