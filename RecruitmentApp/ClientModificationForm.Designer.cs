﻿namespace RecruitmentApp
{
    partial class ClientModificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.emailLabel = new DevExpress.XtraEditors.LabelControl();
            this.addEmailButton = new DevExpress.XtraEditors.SimpleButton();
            this.phoneNumberListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.emailListBoxControl = new DevExpress.XtraEditors.ListBoxControl();
            this.dataLabel = new DevExpress.XtraEditors.LabelControl();
            this.clientModiLayoutTable = new System.Windows.Forms.TableLayoutPanel();
            this.nameLabel = new DevExpress.XtraEditors.LabelControl();
            this.nameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.surnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.surnameLabel = new DevExpress.XtraEditors.LabelControl();
            this.addressLabel = new DevExpress.XtraEditors.LabelControl();
            this.statusLabel = new DevExpress.XtraEditors.LabelControl();
            this.addressTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.statusComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panel1 = new System.Windows.Forms.Panel();
            this.deleteButton = new DevExpress.XtraEditors.SimpleButton();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.saveButton = new DevExpress.XtraEditors.SimpleButton();
            this.formTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.addPhoneNumberButton = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNumberListBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailListBoxControl)).BeginInit();
            this.clientModiLayoutTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.surnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusComboBoxEdit.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.formTableLayoutPanel.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.emailLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.addEmailButton, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 149);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(390, 30);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // emailLabel
            // 
            this.emailLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.emailLabel.Location = new System.Drawing.Point(3, 7);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.emailLabel.Size = new System.Drawing.Size(62, 16);
            this.emailLabel.TabIndex = 0;
            this.emailLabel.Text = "Email(s):";
            // 
            // addEmailButton
            // 
            this.addEmailButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.addEmailButton.Location = new System.Drawing.Point(327, 3);
            this.addEmailButton.Margin = new System.Windows.Forms.Padding(3, 3, 13, 3);
            this.addEmailButton.Name = "addEmailButton";
            this.addEmailButton.Size = new System.Drawing.Size(50, 24);
            this.addEmailButton.TabIndex = 1;
            this.addEmailButton.Text = "Add";
            this.addEmailButton.Click += new System.EventHandler(this.addEmailButton_Click);
            // 
            // phoneNumberListBoxControl
            // 
            this.phoneNumberListBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.phoneNumberListBoxControl.Location = new System.Drawing.Point(10, 272);
            this.phoneNumberListBoxControl.Margin = new System.Windows.Forms.Padding(10, 3, 13, 3);
            this.phoneNumberListBoxControl.Name = "phoneNumberListBoxControl";
            this.phoneNumberListBoxControl.Size = new System.Drawing.Size(367, 54);
            this.phoneNumberListBoxControl.TabIndex = 7;
            this.phoneNumberListBoxControl.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.phoneNumberListBoxControl_MouseDoubleClick);
            // 
            // emailListBoxControl
            // 
            this.emailListBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.emailListBoxControl.Location = new System.Drawing.Point(10, 182);
            this.emailListBoxControl.Margin = new System.Windows.Forms.Padding(10, 3, 13, 3);
            this.emailListBoxControl.Name = "emailListBoxControl";
            this.emailListBoxControl.Size = new System.Drawing.Size(367, 54);
            this.emailListBoxControl.TabIndex = 6;
            this.emailListBoxControl.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.emailListBoxControl_MouseDoubleClick);
            // 
            // dataLabel
            // 
            this.dataLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dataLabel.Location = new System.Drawing.Point(3, 7);
            this.dataLabel.Name = "dataLabel";
            this.dataLabel.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.dataLabel.Size = new System.Drawing.Size(88, 16);
            this.dataLabel.TabIndex = 4;
            this.dataLabel.Text = "Personal data:";
            // 
            // clientModiLayoutTable
            // 
            this.clientModiLayoutTable.ColumnCount = 2;
            this.clientModiLayoutTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.clientModiLayoutTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.clientModiLayoutTable.Controls.Add(this.nameLabel, 0, 0);
            this.clientModiLayoutTable.Controls.Add(this.nameTextEdit, 1, 0);
            this.clientModiLayoutTable.Controls.Add(this.surnameTextEdit, 1, 1);
            this.clientModiLayoutTable.Controls.Add(this.surnameLabel, 0, 1);
            this.clientModiLayoutTable.Controls.Add(this.addressLabel, 0, 2);
            this.clientModiLayoutTable.Controls.Add(this.statusLabel, 0, 3);
            this.clientModiLayoutTable.Controls.Add(this.addressTextEdit, 1, 2);
            this.clientModiLayoutTable.Controls.Add(this.statusComboBoxEdit, 1, 3);
            this.clientModiLayoutTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clientModiLayoutTable.Location = new System.Drawing.Point(0, 30);
            this.clientModiLayoutTable.Margin = new System.Windows.Forms.Padding(0);
            this.clientModiLayoutTable.Name = "clientModiLayoutTable";
            this.clientModiLayoutTable.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.clientModiLayoutTable.RowCount = 5;
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.clientModiLayoutTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.clientModiLayoutTable.Size = new System.Drawing.Size(390, 119);
            this.clientModiLayoutTable.TabIndex = 1;
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.nameLabel.Location = new System.Drawing.Point(13, 7);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(38, 16);
            this.nameLabel.TabIndex = 0;
            this.nameLabel.Text = "Name:";
            // 
            // nameTextEdit
            // 
            this.nameTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameTextEdit.Location = new System.Drawing.Point(161, 3);
            this.nameTextEdit.Name = "nameTextEdit";
            this.nameTextEdit.Properties.Mask.EditMask = "\\p{L}+";
            this.nameTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.nameTextEdit.Size = new System.Drawing.Size(216, 22);
            this.nameTextEdit.TabIndex = 1;
            this.nameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.nameTextEdit_Validating);
            // 
            // surnameTextEdit
            // 
            this.surnameTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.surnameTextEdit.Location = new System.Drawing.Point(161, 33);
            this.surnameTextEdit.Name = "surnameTextEdit";
            this.surnameTextEdit.Properties.Mask.EditMask = "\\p{L}+";
            this.surnameTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.surnameTextEdit.Size = new System.Drawing.Size(216, 22);
            this.surnameTextEdit.TabIndex = 2;
            this.surnameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.surnameTextEdit_Validating);
            // 
            // surnameLabel
            // 
            this.surnameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.surnameLabel.Location = new System.Drawing.Point(13, 37);
            this.surnameLabel.Name = "surnameLabel";
            this.surnameLabel.Size = new System.Drawing.Size(57, 16);
            this.surnameLabel.TabIndex = 3;
            this.surnameLabel.Text = "Surname:";
            // 
            // addressLabel
            // 
            this.addressLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.addressLabel.Location = new System.Drawing.Point(13, 67);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(51, 16);
            this.addressLabel.TabIndex = 4;
            this.addressLabel.Text = "Address:";
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.statusLabel.Location = new System.Drawing.Point(13, 97);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(41, 16);
            this.statusLabel.TabIndex = 7;
            this.statusLabel.Text = "Status:";
            // 
            // addressTextEdit
            // 
            this.addressTextEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addressTextEdit.Location = new System.Drawing.Point(161, 63);
            this.addressTextEdit.Name = "addressTextEdit";
            this.addressTextEdit.Size = new System.Drawing.Size(216, 22);
            this.addressTextEdit.TabIndex = 8;
            this.addressTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.addressTextEdit_Validating);
            // 
            // statusComboBoxEdit
            // 
            this.statusComboBoxEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusComboBoxEdit.Location = new System.Drawing.Point(161, 93);
            this.statusComboBoxEdit.Name = "statusComboBoxEdit";
            this.statusComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.statusComboBoxEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.statusComboBoxEdit.Size = new System.Drawing.Size(216, 22);
            this.statusComboBoxEdit.TabIndex = 11;
            this.statusComboBoxEdit.SelectedValueChanged += new System.EventHandler(this.statusComboBoxEdit_SelectedValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.deleteButton);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.saveButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 332);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 91);
            this.panel1.TabIndex = 3;
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(180, 49);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(94, 29);
            this.deleteButton.TabIndex = 2;
            this.deleteButton.Text = "Delete";
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(280, 14);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(94, 29);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(180, 14);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(94, 29);
            this.saveButton.TabIndex = 0;
            this.saveButton.Text = "Save";
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // formTableLayoutPanel
            // 
            this.formTableLayoutPanel.ColumnCount = 1;
            this.formTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.formTableLayoutPanel.Controls.Add(this.tableLayoutPanel2, 0, 4);
            this.formTableLayoutPanel.Controls.Add(this.panel1, 0, 6);
            this.formTableLayoutPanel.Controls.Add(this.clientModiLayoutTable, 0, 1);
            this.formTableLayoutPanel.Controls.Add(this.dataLabel, 0, 0);
            this.formTableLayoutPanel.Controls.Add(this.emailListBoxControl, 0, 3);
            this.formTableLayoutPanel.Controls.Add(this.phoneNumberListBoxControl, 0, 5);
            this.formTableLayoutPanel.Controls.Add(this.tableLayoutPanel1, 0, 2);
            this.formTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.formTableLayoutPanel.Name = "formTableLayoutPanel";
            this.formTableLayoutPanel.RowCount = 7;
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.formTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.formTableLayoutPanel.Size = new System.Drawing.Size(390, 426);
            this.formTableLayoutPanel.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.addPhoneNumberButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 239);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(390, 30);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelControl1.Location = new System.Drawing.Point(3, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.labelControl1.Size = new System.Drawing.Size(114, 16);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Phone number(s):";
            // 
            // addPhoneNumberButton
            // 
            this.addPhoneNumberButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.addPhoneNumberButton.Location = new System.Drawing.Point(327, 3);
            this.addPhoneNumberButton.Margin = new System.Windows.Forms.Padding(3, 3, 13, 3);
            this.addPhoneNumberButton.Name = "addPhoneNumberButton";
            this.addPhoneNumberButton.Size = new System.Drawing.Size(50, 24);
            this.addPhoneNumberButton.TabIndex = 1;
            this.addPhoneNumberButton.Text = "Add";
            this.addPhoneNumberButton.Click += new System.EventHandler(this.addPhoneNumberButton_Click);
            // 
            // ClientModificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(390, 426);
            this.Controls.Add(this.formTableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ClientModificationForm";
            this.Text = "ClientModificationForm";
            this.Shown += new System.EventHandler(this.ClientModificationForm_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.phoneNumberListBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailListBoxControl)).EndInit();
            this.clientModiLayoutTable.ResumeLayout(false);
            this.clientModiLayoutTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.surnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusComboBoxEdit.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.formTableLayoutPanel.ResumeLayout(false);
            this.formTableLayoutPanel.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl emailLabel;
        private DevExpress.XtraEditors.SimpleButton addEmailButton;
        private DevExpress.XtraEditors.ListBoxControl phoneNumberListBoxControl;
        private DevExpress.XtraEditors.ListBoxControl emailListBoxControl;
        private DevExpress.XtraEditors.LabelControl dataLabel;
        private System.Windows.Forms.TableLayoutPanel clientModiLayoutTable;
        private DevExpress.XtraEditors.LabelControl nameLabel;
        private DevExpress.XtraEditors.TextEdit nameTextEdit;
        private DevExpress.XtraEditors.TextEdit surnameTextEdit;
        private DevExpress.XtraEditors.LabelControl surnameLabel;
        private DevExpress.XtraEditors.LabelControl addressLabel;
        private DevExpress.XtraEditors.LabelControl statusLabel;
        private DevExpress.XtraEditors.TextEdit addressTextEdit;
        private DevExpress.XtraEditors.ComboBoxEdit statusComboBoxEdit;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.SimpleButton saveButton;
        private System.Windows.Forms.TableLayoutPanel formTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton addPhoneNumberButton;
        private DevExpress.XtraEditors.SimpleButton deleteButton;
    }
}